﻿using bART_BLL.Interfaces;
using bART_BLL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bART_Solutions_Task.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        //Inject book service via constructor

        private readonly IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        //GET
        [HttpGet]
        public ActionResult<IEnumerable<AccountModel>> GetAllContacts()
        {
            var s = _accountService.GetAll();
            return Ok(s);
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<AccountModel>> GetAccountByName(string name)
        {
            try
            {
                var s = await _accountService.GetByPrimaryKeyAsync(name);
                return Ok(s);
            }
            catch(Exception e)
            {
                return NotFound(e.Message);
            }
        }

        //POST
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] AccountModel accountModel)
        {
            try
            {
                await _accountService.AddAsync(accountModel);
                var account = _accountService.GetAll().Last();
                return Ok(account);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
