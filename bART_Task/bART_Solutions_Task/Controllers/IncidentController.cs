﻿using bART_BLL.Interfaces;
using bART_BLL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bART_Solutions_Task.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IncidentController : ControllerBase
    {
        //Inject book service via constructor

        private readonly IIncidentService _incidentService;
        public IncidentController(IIncidentService incidentService)
        {
            _incidentService = incidentService;
        }

        //GET
        [HttpGet]
        public ActionResult<IEnumerable<IncidentModel>> GetAllContacts()
        {
            var s = _incidentService.GetAll();
            return Ok(s);
        }

        //POST
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] IncidentModel incidentModel)
        {
            try
            {
                await _incidentService.AddAsync(incidentModel);
                var incident = _incidentService.GetAll().Last();
                return Ok(incident);
            }
            catch (Exception e)
            {
                return StatusCode(400, e);

            }
        }
    }
}
