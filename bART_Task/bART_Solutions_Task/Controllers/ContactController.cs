﻿using bART_BLL.Interfaces;
using bART_BLL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bART_Solutions_Task.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        //Inject book service via constructor

        private readonly IContactService _contactService;
        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        //GET
        [HttpGet]
        public ActionResult<IEnumerable<ContactModel>> GetAllContacts()
        {
            var s = _contactService.GetAll();
            return Ok(s);
        }

        //GET
        [HttpGet("{email}")]
        public ActionResult<IEnumerable<ContactModel>> GetContactByEmail(string email)
        {
            try
            {
                var s = _contactService.GetByPrimaryKeyAsync(email);
                return Ok(s);
            }
            catch(Exception e)
            {
                return NotFound(e.Message);
            }
        }

        //POST
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] ContactModel contactModel)
        {
            try
            {
                await _contactService.AddAsync(contactModel);
                var contact = _contactService.GetAll().Last();
                return Ok(contact);
            }
            catch (Exception e)
            {
                return StatusCode(400, e);
                //return HttpStatusCode.NotFound;
                //Request.CreateResponse(HttpStatusCode.OK, e);
            }
        }

        [HttpPut]
        public async Task<ActionResult> LinkContactToAccount(ContactModel model)
        {
            await _contactService.UpdateAsync(model);
            return Ok();
        }
    }
}
