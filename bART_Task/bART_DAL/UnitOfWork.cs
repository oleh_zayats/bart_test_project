﻿using bART_DAL.Interfaces;
using bART_DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly bART_DbContext _context;
        private ContactRepository _contactRepository;
        private AccountRepository _accountRepository;
        private IncidentRepository _incidentRepository;

        public UnitOfWork(bART_DbContext context)
        {
            this._context = context;
        }
        public IContactRepository ContactRepository
        {
            get
            {
                if (this._contactRepository == null)
                {
                    this._contactRepository = new ContactRepository(_context);
                }
                return _contactRepository;
            }

        }
        public IAccountRepository AccountRepository
        {
            get
            {
                if (this._accountRepository == null)
                {
                    this._accountRepository = new AccountRepository(_context);
                }
                return _accountRepository;
            }

        }

        public IIncidentRepository IncidentRepository
        {
            get
            {
                if (this._incidentRepository == null)
                {
                    this._incidentRepository = new IncidentRepository(_context);
                }
                return _incidentRepository;
            }

        }


        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
