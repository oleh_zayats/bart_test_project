﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bART_DAL.Entities;

namespace bART_DAL.Interfaces
{
    public interface IContactRepository : IRepository<Contact>
    {
    }
}
