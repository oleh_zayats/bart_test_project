﻿using System.Threading.Tasks;

namespace bART_DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IContactRepository ContactRepository { get; }

        IAccountRepository AccountRepository { get; }

        IIncidentRepository IncidentRepository { get; }

        Task<int> SaveAsync();
    }
}