﻿using bART_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_DAL.Interfaces
{
    public interface IAccountRepository : IRepository<Account>
    {
    }
}
