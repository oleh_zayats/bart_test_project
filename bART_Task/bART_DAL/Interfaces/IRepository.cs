﻿using bART_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : IBaseEntity
    {
        IQueryable<TEntity> FindAll();

        Task<TEntity> GetByPrimaryKeyAsync(string key);

        Task AddAsync(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        Task DeleteByPrimaryKeyAsync(string key);

        //IEnumerable<Student> GetStudents();
        //Student GetStudentByID(int studentId);
        //void InsertStudent(Student student);
        //void DeleteStudent(int studentID);
        //void UpdateStudent(Student student);
        //void Save();
    }
}
