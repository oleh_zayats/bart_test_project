﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_DAL.Entities
{
    public class Account : IBaseEntity
    {
        [Key]
        public string Name { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
