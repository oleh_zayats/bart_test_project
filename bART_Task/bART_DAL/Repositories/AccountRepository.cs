﻿using bART_DAL.Entities;
using bART_DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_DAL.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        protected readonly bART_DbContext contex;
        protected readonly DbSet<Account> entities;

        public AccountRepository(bART_DbContext contex)
        {
            this.contex = contex;
            entities = contex.Set<Account>();
        }

        public async Task AddAsync(Account entity)
        {
            await entities.AddAsync(entity);
        }

        public void Delete(Account entity)
        {
            entities.Remove(entity);
        }

        public async Task DeleteByPrimaryKeyAsync(string key)
        {
            var toDelete = await entities.FindAsync(key);
            entities.Remove(toDelete);
        }

        public IQueryable<Account> FindAll()
        {
            return entities.AsQueryable();
        }

        public async Task<Account> GetByPrimaryKeyAsync(string key)
        {
            return await entities.FindAsync(key);
        }

        public void Update(Account entity)
        {
            entities.Update(entity);
        }
    }
}
