﻿using bART_DAL.Entities;
using bART_DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_DAL.Repositories
{
    public class IncidentRepository : IIncidentRepository
    {
        protected readonly bART_DbContext contex;
        protected readonly DbSet<Incident> entities;

        public IncidentRepository(bART_DbContext contex)
        {
            this.contex = contex;
            entities = contex.Set<Incident>();
        }

        public async Task AddAsync(Incident entity)
        {
            await entities.AddAsync(entity);
        }

        public void Delete(Incident entity)
        {
            entities.Remove(entity);
        }

        public async Task DeleteByPrimaryKeyAsync(string key)
        {
            var toDelete = await entities.FindAsync(key);
            entities.Remove(toDelete);
        }

        public IQueryable<Incident> FindAll()
        {
            return entities.AsQueryable();
        }

        public async Task<Incident> GetByPrimaryKeyAsync(string key)
        {
            return await entities.FindAsync(key);
        }

        public void Update(Incident entity)
        {
            entities.Update(entity);
        }
    }
}
