﻿using bART_DAL.Entities;
using bART_DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_DAL.Repositories
{
    public class ContactRepository : IContactRepository
    {
        protected readonly bART_DbContext contex;
        protected readonly DbSet<Contact> entities;

        public ContactRepository(bART_DbContext contex)
        {
            this.contex = contex;
            entities = contex.Set<Contact>();
        }

        public async Task AddAsync(Contact entity)
        {
            await entities.AddAsync(entity);          
        }

        public void Delete(Contact entity)
        {
            entities.Remove(entity);
        }

        public async Task DeleteByPrimaryKeyAsync(string key)
        {
            var toDelete = await entities.FindAsync(key);
            entities.Remove(toDelete);
        }

        public IQueryable<Contact> FindAll()
        {
            return entities.AsQueryable();
        }

        public async Task<Contact> GetByPrimaryKeyAsync(string key)
        {
            return await entities.FindAsync(key);
        }

        public void Update(Contact entity)
        {
            entities.Update(entity);
        }
    }
}
