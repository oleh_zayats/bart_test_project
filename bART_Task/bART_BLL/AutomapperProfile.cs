﻿using AutoMapper;
using bART_BLL.Models;
using bART_DAL.Entities;
using System.Linq;

namespace Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Account, AccountModel>()
                .ForMember(dest => dest.Contacts, options => options.MapFrom(source => source.Contacts))
                .ReverseMap();

            //Mapping for card and card model
            CreateMap<Contact, ContactModel>()
                .ReverseMap();
            //Mapping that combines Reader and ReaderProfile into ReaderModel
            CreateMap<Incident, IncidentModel>()
                .ForMember(dest => dest.Accounts, options=>options.MapFrom(source=>source.Accounts))
                .ReverseMap();


            //Before doing reader mapping, learn more about projection in AutoMapper.
            //https://docs.automapper.org/en/stable/Projection.html
            //https://www.infoworld.com/article/3192900/how-to-work-with-automapper-in-csharp.html
        }
    }
}