﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_BLL.Interfaces
{
    public interface ICrud<TModel> where TModel : class
    {
        IEnumerable<TModel> GetAll();

        Task<TModel> GetByPrimaryKeyAsync(string key);

        Task AddAsync(TModel model);

        Task UpdateAsync(TModel model);

        Task DeleteByPrimaryKeyAsync(string key);
    }
}
