﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_BLL.Models
{
    public class AccountModel
    {
        public string Name { get; set; }
        public ICollection<ContactModel> Contacts { get; set; }
    }
}
