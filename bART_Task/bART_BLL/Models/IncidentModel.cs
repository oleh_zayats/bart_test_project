﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_BLL.Models
{
    public class IncidentModel
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<AccountModel> Accounts { get; set; }
    }
}
