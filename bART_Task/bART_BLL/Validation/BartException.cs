﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace bART_BLL.Validation
{
    [Serializable]
    public class BartException : Exception
    {
        public BartException() { }

        public BartException(string name)
            : base($"{name}")
        {

        }

        protected BartException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}


