﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using bART_BLL.Interfaces;
using bART_BLL.Models;
using bART_DAL.Interfaces;
using bART_DAL.Entities;
using bART_BLL.Validation;

namespace bART_BLL.Services
{
    public class ContactService : IContactService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ContactService(IUnitOfWork unit, IMapper mapper)
        {
            this._mapper = mapper;
            this._unitOfWork = unit;
        }
        public async Task AddAsync(ContactModel model)
        {
            var newAccount = _mapper.Map<Contact>(model);
            await _unitOfWork.ContactRepository.AddAsync(newAccount);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByPrimaryKeyAsync(string key)
        {
            await _unitOfWork.ContactRepository.DeleteByPrimaryKeyAsync(key);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<ContactModel> GetAll()
        {
            List<ContactModel> contactModels = new List<ContactModel>();
            var list = _unitOfWork.ContactRepository.FindAll();
            foreach (var contact in list)
            {
                contactModels.Add(_mapper.Map<ContactModel>(contact));
            }
            return contactModels;
        }

        public async Task<ContactModel> GetByPrimaryKeyAsync(string key)
        {
            var contact = await _unitOfWork.ContactRepository.GetByPrimaryKeyAsync(key);
            if(contact == null)
            {
                throw new BartException("Contact not found!");
            }
            return _mapper.Map<ContactModel>(contact);
        }

        public async Task UpdateAsync(ContactModel model)
        {
            _unitOfWork.ContactRepository.Update(_mapper.Map<Contact>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}
