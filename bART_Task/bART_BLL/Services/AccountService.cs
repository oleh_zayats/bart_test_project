﻿using AutoMapper;
using bART_BLL.Interfaces;
using bART_BLL.Models;
using bART_BLL.Validation;
using bART_DAL.Entities;
using bART_DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bART_BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public AccountService(IUnitOfWork unit, IMapper mapper)
        {
            this._mapper = mapper;
            this._unitOfWork = unit;
        }
        public async Task AddAsync(AccountModel model)
        {
            var newAccount = _mapper.Map<Account>(model);
            if(newAccount.Contacts.Count == 0)
            {
                throw new BartException("Account cannot be created without contact");
            }
            for (int i = 0; i < newAccount.Contacts.Count; i++)
            {
                var contact = await _unitOfWork.ContactRepository.GetByPrimaryKeyAsync(newAccount.Contacts.ToArray()[i].Email);
                if(contact != null)
                {
                    _unitOfWork.ContactRepository.Delete(contact);
                    await _unitOfWork.SaveAsync();
                }
            }
            await _unitOfWork.AccountRepository.AddAsync(newAccount);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByPrimaryKeyAsync(string key)
        {
            await _unitOfWork.AccountRepository.DeleteByPrimaryKeyAsync(key);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<AccountModel> GetAll()
        {
            List<AccountModel> accountModels = new List<AccountModel>();
            var list = _unitOfWork.AccountRepository.FindAll();
            foreach (var account in list)
            {
                accountModels.Add(_mapper.Map<AccountModel>(account));
            }
            return accountModels;
        }

        public async Task<AccountModel> GetByPrimaryKeyAsync(string key)
        {
            var account = await _unitOfWork.AccountRepository.GetByPrimaryKeyAsync(key);
            if (account == null)
            {
                throw new BartException("Account not found!");
            }
            return _mapper.Map<AccountModel>(account);
        }

        public async Task UpdateAsync(AccountModel model)
        {
            _unitOfWork.AccountRepository.Update(_mapper.Map<Account>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}
