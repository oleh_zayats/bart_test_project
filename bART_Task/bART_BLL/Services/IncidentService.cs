﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using bART_BLL.Interfaces;
using bART_BLL.Models;
using bART_DAL.Interfaces;
using bART_DAL.Entities;

namespace bART_BLL.Services
{
    public class IncidentService : IIncidentService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public IncidentService(IUnitOfWork unit, IMapper mapper)
        {
            this._mapper = mapper;
            this._unitOfWork = unit;
        }
        public async Task AddAsync(IncidentModel model)
        {
            var newAccount = _mapper.Map<Incident>(model);
            await _unitOfWork.IncidentRepository.AddAsync(newAccount);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByPrimaryKeyAsync(string key)
        {
            await _unitOfWork.IncidentRepository.DeleteByPrimaryKeyAsync(key);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<IncidentModel> GetAll()
        {
            List<IncidentModel> incidentModels = new List<IncidentModel>();
            var list = _unitOfWork.IncidentRepository.FindAll();
            foreach (var incident in list)
            {
                incidentModels.Add(_mapper.Map<IncidentModel>(incident));
            }
            return incidentModels;
        }

        public async Task<IncidentModel> GetByPrimaryKeyAsync(string key)
        {
            var incident = await _unitOfWork.IncidentRepository.GetByPrimaryKeyAsync(key);
            return _mapper.Map<IncidentModel>(incident);
        }

        public async Task UpdateAsync(IncidentModel model)
        {
            _unitOfWork.IncidentRepository.Update(_mapper.Map<Incident>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}
